<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelPenjualanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penjualan_detail', function(Blueprint $table) {
            $table->increments('id_penjualan_detail');
            $table->integer('id_penjualan')->unsigned();
            $table->integer('id_produk')->unsigned();
            $table->bigInteger('harga_jual');
            $table->integer('jumlah');
            $table->integer('diskon');
            $table->bigInteger('sub_total');
            $table->timestamps();
        });

        Schema::table('penjualan_detail', function(Blueprint $table) {
            $table->foreign('id_penjualan')->references('id_penjualan')->on('penjualan')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('penjualan_detail', function(Blueprint $table) {
            $table->foreign('id_produk')->references('id_produk')->on('produk')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penjualan_detail');
    }
}
