<?php

Route::group(['middleware' => ['web', 'auth', 'cekuser:1']], function() {

	/* --- Home --- */
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/home', 'HomeController@index')->name('home');
	
	/* --- Kategori --- */
	Route::get('kategori/data', 'KategoriController@listData')->name('kategori.data');
	Route::resource('kategori', 'KategoriController');
	
	/* --- Produk --- */
	Route::get('produk/data', 'ProdukController@listData')->name('produk.data');
	Route::post('produk/hapus', 'ProdukController@deleteSelected');
	Route::post('produk/cetak', 'ProdukController@printBarcode');
	Route::get('produk/pdf', 'ProdukController@makePDF');
	Route::post('produkstore', 'ProdukController@store');
	Route::resource('produk', 'ProdukController');

	/* --- Supplier --- */
	Route::get('supplier/data', 'SupplierController@listData')->name('supplier.data');
	Route::resource('supplier', 'SupplierController');

	/* --- Member --- */
	Route::get('member/data', 'MemberController@listData')->name('member.data');
	Route::post('member/cetak', 'MemberController@printCard');
	Route::resource('member', 'MemberController');

	/* --- Pengeluaran --- */
	Route::get('pengeluaran/data', 'PengeluaranController@listData')->name('pengeluaran.data');
	Route::resource('pengeluaran', 'PengeluaranController');

	/* --- User --- */
	Route::get('user/data', 'UserController@listData')->name('user.data');
	Route::resource('user', 'UserController')->except([
		'show'
	]);	

	/* --- Pembelian --- */
	Route::get('pembelian/data', 'PembelianController@listData')->name('pembelian.data');
	Route::get('pembelian/{id}/tambah', 'PembelianController@create');
	Route::get('pembelian/{id}/lihat', 'PembelianController@show');
	Route::resource('pembelian', 'PembelianController')->except([
		'create'
	]);

	/* --- Pembelian Detail --- */
	Route::get('pembelian_detail/{id}/data', 'PembelianDetailController@listData')->name('pembelian_detail.data');
	Route::get('pembelian_detail/loadform/{diskon}/{total}', 'PembelianDetailController@loadForm');
	Route::resource('pembelian_detail', 'PembelianDetailController')->except([
		'create', 'show', 'edit'
	]);

	/* --- Penjualan --- */
	Route::get('penjualan/data', 'PenjualanController@listData')->name('penjualan.data');
	Route::get('penjualan/{id}/lihat', 'PenjualanController@show');
	Route::resource('penjualan', 'PenjualanController')->except([
		'create', 'edit', 'update'
	]);

	/* --- Laporan --- */
	Route::get('laporan', 'LaporanController@index')->name('laporan.index');
	Route::post('laporan', 'LaporanController@refresh')->name('laporan.refresh');
	Route::get('laporan/data/{awal}/{akhir}', 'LaporanController@listData')->name('laporan.data');
	Route::get('laporan/pdf/{awal}/{akhir}', 'LaporanController@exportPDF');

	/* --- Setting --- */
	Route::resource('setting', 'SettingController')->except([
		'show', 'create', 'destroy'
	]);

	/* --- Penjualan Detail --- */
	Route::get('transaksi/baru', 'PenjualanDetailController@newSession')->name('transaksi.new');
	Route::get('transaksi/{id}/data', 'PenjualanDetailController@listData')->name('transaksi.data');
	Route::get('transaksi/cetaknota', 'PenjualanDetailController@printNota')->name('transaksi.cetak');
	Route::get('transaksi/cetaknota/kecil', 'PenjualanDetailController@printNotaKecil')->name('transaksi.notakecil');
	Route::get('transaksi/notapdf', 'PenjualanDetailController@notaPDF')->name('transaksi.pdf');
	Route::post('transaksi/simpan', 'PenjualanDetailController@saveData')->name('transaksi.simpan');
	Route::get('transaksi/loadform/{diskon}/{total}/{diterima}', 'PenjualanDetailController@loadForm');
	Route::resource('transaksi', 'PenjualanDetailController')->except('show');
});

Route::group(['middleware' => ['web', 'auth']], function() {
	Route::get('user/profil', 'UserController@profil')->name('user.profil');
	Route::patch('user/{id}/change', 'UserController@changeProfil')->name('profil.update');

	/* --- Penjualan Detail --- */
	Route::get('transaksi/baru', 'PenjualanDetailController@newSession')->name('transaksi.new');
	Route::get('transaksi/{id}/data', 'PenjualanDetailController@listData')->name('transaksi.data');
	Route::get('transaksi/cetaknota', 'PenjualanDetailController@printNota')->name('transaksi.cetak');
	Route::get('transaksi/cetaknota/kecil', 'PenjualanDetailController@printNotaKecil')->name('transaksi.notakecil');
	Route::get('transaksi/notapdf', 'PenjualanDetailController@notaPDF')->name('transaksi.pdf');
	Route::post('transaksi/simpan', 'PenjualanDetailController@saveData')->name('transaksi.simpan');
	Route::get('transaksi/loadform/{diskon}/{total}/{diterima}', 'PenjualanDetailController@loadForm');
	Route::resource('transaksi', 'PenjualanDetailController')->except('show');
});

Route::get('/', 'HomeController@index')->name('home');
Auth::routes();

