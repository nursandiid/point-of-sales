<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';
    protected $primaryKey = 'id_produk';
    protected $guarded = [];

    public function kategori() {
    	return $this->belongsTo(Kategori::class, 'id_kategori');
    }

    public function pembelian_detail()
    {
        return $this->hasMany(PembelianDetail::class, 'id_produk');
    }
}
