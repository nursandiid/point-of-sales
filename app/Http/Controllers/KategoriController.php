<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;

class KategoriController extends Controller
{
    
    public function index()
    {
        return view('kategori.index');
    }

    
    public function listData() 
    {
        $kategori = Kategori::orderBy('id_kategori', 'desc')->get();
        $no = 0;
        $data = array();

        foreach ($kategori as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->nama_kategori;
            $row[] = '
                    <a onclick="editForm('. $list->id_kategori .')" class="btn btn-xs btn-flat btn-info"><i class="fa fa-pencil"></i> Edit</a>
                    <a onclick="deleteData('. $list->id_kategori .')" class="btn btn-xs btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</a>
            ';
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    
    public function store(Request $request)
    {
        $kategori = new Kategori;
        $kategori->nama_kategori = $request['nama'];
        $kategori->save();
    }

    
    public function edit($id)
    {
        $kategori = Kategori::find($id);
        echo json_encode($kategori);
    }

    
    public function update(Request $request, $id)
    {
        $kategori = Kategori::find($id);
        $kategori->nama_kategori = $request['nama'];
        $kategori->update();
    }

    
    public function destroy($id)
    {
        try {
            $kategori = Kategori::find($id);
            $kategori->delete();
        } catch (\Exception $e) {
            return response()->json(['message' => 'Kategori '. $kategori->nama_kategori. ' masih digunakan']);
        }

    }
}
