<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Pembelian;
use App\Supplier;
use App\Produk;
use App\PembelianDetail;

class PembelianDetailController extends Controller
{
    public function index()
    {
    	$produk = Produk::all();
    	$idpembelian = session('idpembelian');
    	$supplier = Supplier::find(session('idsupplier'));
        return view('pembelian_detail.index', compact('produk', 'idpembelian', 'supplier'));
    }

    public function listData($id)
    {
        $detail = PembelianDetail::with('produk')->where('id_pembelian', '=', $id)->get();

        $no = 0;
        $data = array();
        $total = 0;
        $total_item = 0;

        foreach ($detail as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->produk['kode_produk'];
            $row[] = $list->produk['nama_produk'];
            $row[] = 'Rp. '. format_uang($list->harga_beli);
            $row[] = '
				<input type="number" class="form-control input-sm" name="jumlah'. $list->id_pembelian_detail .'" value="'. $list->jumlah .'" onchange="changeCount('. $list->id_pembelian_detail .')">
            ';
            $row[] = 'Rp. '. format_uang($list->harga_beli * $list->jumlah);
            $row[] = '
                    <a onclick="deleteItem('. $list->id_pembelian_detail .')" class="btn btn-xs btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</a>
            ';
            $data[] = $row;

            $total += $list->harga_beli * $list->jumlah;
            $total_item += $list->jumlah;
        }
        $data[] = [
            '<span class="total hide">'.$total.'</span> <span class="totalitem hide">'.$total_item.'</span>',
            '',
            '',
            '',
            '',
            '',
            ''
        ];

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function store(Request $request)
    {
        $produk = Produk::where('id_produk', '=', $request['id_produk'])->first();
        $detail = new PembelianDetail;
        $detail->id_pembelian = $request['idpembelian'];
        $detail->id_produk = $request['id_produk'];
        $detail->harga_beli = $produk->harga_beli;
        $detail->jumlah = 1;
        $detail->sub_total = $produk->harga_beli; 
        $detail->save();
    }

    public function update(Request $request, $id) 
    {
    	$nama_input = 'jumlah'. $id;
    	$detail = PembelianDetail::find($id);
    	$detail->jumlah = $request[$nama_input];
    	$detail->sub_total = $detail->harga_beli * $request[$nama_input];
    	$detail->update();
    }

    public function destroy($id)
    {
    	$detail = PembelianDetail::find($id);
    	$detail->delete();
    }

    public function loadForm($diskon, $total)
    {
    	$bayar = $total - ($diskon / 100 * $total);
    	$data  = array(
    		'totalrp' => format_uang($total),
    		'bayar' => $bayar,
    		'bayarrp' => format_uang($bayar),
    		'terbilang' => ucwords(terbilang($bayar). ' Rupiah')
    	);

    	return response()->json($data);
    }
}
