<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CekUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $level)
    {
        if(Auth::user() && Auth::user()->level != $level) 
            return redirect('/');
        else return $next($request);
    }
}
