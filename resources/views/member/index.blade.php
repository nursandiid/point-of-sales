@extends('layouts.app')

@section('title')
	Daftar Member
@endsection

@section('breadcrumb')
	@parent
	<li>Member</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
			    <div class="box-header with-border">
			        <a onclick="addForm()" class="btn btn-success btn-xs btn-flat"><i class="fa fa-plus-circle"></i> Tambah</a>
			        <a onclick="printCard()" class="btn btn-info btn-xs btn-flat"><i class="fa fa-credit-card"></i> Cetak Kartu</a>
			    </div>
			
			    <form method="post" id="form-member"> 
			    	@csrf
			    	<div class="box-body" style="overflow: auto;">
			        	<table class="table table-striped table-bordered">
				            <thead>
				            	<th width="20"><input type="checkbox" value="1" id="select-all" style="cursor: pointer;"></th>
				                <th width="30">No</th>
				                <th>Kode Member</th>
				                <th>Nama Member</th>
				                <th>Alamat</th>
				                <th>Telpon</th>
				                <th width="100">Aksi</th>
				            </thead>
				            <tbody></tbody>
				        </table>
			    	</div>
			    </form>
			</div>
		</div>
	</div>
	@include('member.form')
@endsection

@section('script')
	<script>
		var table, save_method;
		$(function() {
			table = $('.table').DataTable({
				'processing' : true,
				'autoWidth' : false,
				'ajax' : {
					'url' : '{{ route('member.data') }}',
					'type' : 'GET'
				},
				columnDefs : [{
					'targets' : 0,
					'searchable' : false,
					'orderable' : false
				}],
				'order' : [1, 'asc']
			});
		})

		$('#select-all').click(function() {
			$(':checkbox').prop('checked', this.checked);
		})

		$(function() {
			$('#modal-form').validator().on('submit', function(e) {
				if(!e.preventDefault()) {
					var id = $('#id').val();
					
					if(save_method == 'add') url = '{{ route("member.store") }}';
					else url = 'member/' + id;

					$.ajax({
						url : url,
						type : 'POST',
						data : $('#modal-form form').serialize(),
						success : function(data) {
							$('#modal-form').modal('hide');
							table.ajax.reload();
						}
					})

					return false;
				}
			})
		})

		function passwordRandom() {
			return Math.ceil(Math.random() * 99999999999)
		}

		function addForm() {
			save_method = 'add';
			$('#modal-form').modal('show');
			$('input[name=_method]').val('POST');
			$('#modal-form form')[0].reset();
			$('.modal-title').text('Tambah member');
			$('#modal-form form #nama').focus();
			$('#kode').val(passwordRandom());
		}

		function editForm(id) {
			save_method = 'edit';
			$('input[name=_method]').val('PATCH');
			$('#modal-form form')[0].reset();

			$.ajax({
				url : 'member/' + id + '/edit',
				type : 'GET',
				dataType : 'JSON',
				success : function(data) {
					$('#modal-form').modal('show');
					$('.modal-title').text('Edit member');

					$('#id').val(data.id_member);
					$('#kode').val(data.kode_member);
					$('#nama').val(data.nama);
					$('#alamat').val(data.alamat);
					$('#telpon').val(data.telpon);
				}, 
				error : function() {
					alert('Tidak dapat menampilkan data');
				}
			})
		}

		function deleteData(id) {
			if(confirm('Apakah yakin data akan dihapus?')) {
				$.ajax({
					url : 'member/' + id,
					type : 'POST',
					data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
					success : function(data) {
						table.ajax.reload();
					},
					error : function() {
						alert('Tidak dapat menghapus data!');
					}
				})
			}
		}

		function printCard() {
			if($('input:checked').length < 1) {
				alert('Pilih data yang dicetak');
			} else {
				$('#form-member').attr('target', '_blank').attr('action', 'member/cetak').submit();
			}
		}
	</script>
@endsection