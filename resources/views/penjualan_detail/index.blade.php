@extends('layouts.app')

@section('css')
<style>
	#tampil-bayar {
		font-size: 5em; 
		text-align: center; 
		height: 100px;
	}	

	#tampil-terbilang {
		padding: 10px; 
		background-color: #f0f0f0;
	}

	@media(max-width: 768px) {
		#tampil-bayar {
			font-size: 3em;
			white-space: nowrap;
			height: 70px;
			padding-top: 5px;
			overflow: auto;
		}
	}
</style>
@endsection

@section('title')
	Transaksi Penjualan
@endsection

@section('breadcrumb')
	@parent
	<li>Penjualan</li>
	<li>Tambah</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
			    <div class="box-body">
			        <form class="fom-horizontal form-produk" method="post">
						@csrf {{ method_field('POST') }}
						<input type="hidden" name="idpenjualan" value="{{ $idpenjualan }}">

						<div class="form-group row">
							<label for="kode" class="col-md-2 control-label">Kode Produk</label>
							<div class="col-md-5">
								<div class="input-group">
									<input type="hidden" name="id_produk" id="id_produk">
									<input type="text" name="kode" id="kode" class="form-control" onkeypress="showProduct()">
									<span class="input-group-btn">
										<button onclick="showProduct()" type="button" class="btn btn-info btn-flat"><i class="fa fa-search"></i> </button>
									</span>
								</div>
							</div>
						</div>
					</form>

					<form class="form-keranjang table-responsive" method="post">
						@csrf @method('PATCH')
						<table class="table table-striped table-bordered tabel-penjualan">
						    <thead>
						    	<th width="20">No</th>
						        <th>Kode Produk</th>
						        <th>Nama Produk</th>
						        <th align="right">Harga</th>
						        <th>Jumlah</th>
						        <th>Diskon</th>
						        <th align="right">Sub Total</th>
						        <th width="100">Aksi</th>
						    </thead>
						    <tbody></tbody>
						</table> <br>
						
						<style>
							.tabel-penjualan tbody tr:last-child {
								display: none;
							}
						</style>
					</form>

					<div class="col-md-8" style="padding: 0; margin: 0;">
						<div id="tampil-bayar" class="bg-primary"></div>
						<div id="tampil-terbilang"></div>
					</div>

					<div class="col-md-4">
						<form class="fom-horizontal form-penjualan" method="post" action="{{ route('transaksi.simpan') }}">
							@csrf @method('POST')
							<input type="hidden" name="idpenjualan" value="{{ $idpenjualan }}">
							<input type="hidden" name="total" id="total">
							<input type="hidden" name="totalitem" id="totalitem">
							<input type="hidden" name="bayar" id="bayar">

							<div class="form-group row">
								<label for="totalrp" class="col-md-4 control-label">Total</label>
								<div class="col-md-8">
									<input type="text" name="totalrp" id="totalrp" class="form-control" readonly>
								</div>
							</div>

							<div class="form-group row">
								<label for="member" class="col-md-4 control-label">Member</label>
								<div class="col-md-8">
									<div class="input-group">
										<input type="hidden" name="id_member" id="id_member">
										<input type="text" name="member" id="member" class="form-control" value="0">
										<span class="input-group-btn"><button onclick="showMember()" type="button" class="btn btn-info btn-flat"><i class="fa fa-arrow-right"></i></button></span>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label for="diskon" class="col-md-4 control-label">Diskon</label>
								<div class="col-md-8">
									<input type="text" name="diskon" id="diskon" class="form-control" value="0" readonly>
								</div>
							</div>

							<div class="form-group row">
								<label for="bayarrp" class="col-md-4 control-label">Bayar</label>
								<div class="col-md-8">
									<input type="text" name="bayarrp" id="bayarrp" class="form-control" readonly>
								</div>
							</div>

							<div class="form-group row">
								<label for="diterima" class="col-md-4 control-label">Diterima</label>
								<div class="col-md-8">
									<input type="number" name="diterima" id="diterima" class="form-control">
								</div>
							</div>

							<div class="form-group row">
								<label for="kembali" class="col-md-4 control-label">Kembali</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="kembali" value="0" readonly>
								</div>
							</div>
						</form>
					</div>
			    </div>

			    <div class="box-footer">
					<button type="submit" class="btn btn-primary btn-sm btn-flat pull-right simpan"><i class="fa fa-floppy-o"></i> Simpan Transaksi</button>
				</div>
			</div>
		</div>
	</div>
	@include('penjualan_detail.produk')
	@include('penjualan_detail.member')
@endsection

@section('script')
	<script>
		var table;
		
		$(function() {
			$('.tabel-produk').DataTable();
			table = $('.tabel-penjualan').DataTable({
				'dom' : 'Brt',
				'bSort' : false,
				'serverSide' : true,
				'autoWidth' : false,
				'ajax' : {
					'url' : '{{ route('transaksi.data', $idpenjualan) }}',
					'type' : 'GET'
				}
			}).on('draw.dt', function() {
				loadForm($('#diskon').val());
			})
		})

		$('body').addClass('sidebar-collapse')
		$('.form-produk, .form-keranjang').on('submit', function() {
			return false;
		})

		$('#kode').change(function() {
			addItem();
		})

		$('#member').change(function() {
			selectMember($(this).val())
		})

		// Jalankan fungsi loadForm() ketika diterima diubah
		$('#diterima').change(function() {
			if ($(this).val() == '') $(this).val(0).select()
			loadForm($('#diskon').val(), $(this).val())
		}).focus(function() {
			$(this).select()
		})

		// menyimpan form transaksi
		$('.simpan').click(function() {
			$('.form-penjualan').submit();
		})

		function addItem() {
			$.ajax({
				url : '{{ route('transaksi.store') }}',
				type : 'POST',
				data : $('.form-produk').serialize(),
				success : function(data) {
					$('#kode').val('').focus();
					table.ajax.reload(function(){
						loadForm($('#diskon').val())
					})
				}, 
				error : function() {
					alert('Tidak dapat menyimpan data!');
				}
			})
		}

		function showProduct() {
			$('#modal-produk').modal('show');
		}

		function showMember() {
			$('#modal-member').modal('show');
		}

		function selectItem(id, kode) {
			$('#id_produk').val(id);
			$('#kode').val(kode);
			$('#modal-produk').modal('hide');
			addItem();
		}

		function changeCount(id, stok) {
			// if($('#count').val() > stok) {
			// 	alert('Stok tersisa '+ stok);
			// 	$('#count').val('')
			// }

			$.ajax({
				url  : 'transaksi/'+ id,
				type : 'POST',
				data : $('.form-keranjang').serialize(),
				success : function(data) {
					table.ajax.reload(function() {
						loadForm($('#diskon').val());
					})
				}, 
				error : function() {
					alert('Tidak dapat menyimpan data!');
				}
			})
		}

		function selectMember(id, kode) {
			$('#modal-member').modal('hide')
			$('#diskon').val('{{ $setting[0]->diskon_member }}')
			$('#id_member').val(id)
			$('#member').val(kode)
			loadForm($('#diskon')).val()
			$('#diterima').val(0).focus().select()
		}

		function deleteItem(id) {
			if(confirm('Apakah yakin data akan dihapus?')) {
				$.ajax({
					url : 'transaksi/'+ id,
					type : 'POST',
					data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
					success :  function(data) {
						table.ajax.reload(function() {
							loadForm($('#diskon'));
						})
					},
					error : function(data) {
						alert('Tidak dapat menghapus data!');
					}
				})
			}
		}

		function loadForm(diskon = 0, diterima = 0) {
			$('#total').val($('.total').text());
			$('#totalitem').val($('.totalitem').text());
			
			if(diskon) diskon = diskon;
			else diskon = 0;

			$.ajax({
				url : 'transaksi/loadform/' + diskon + '/' + $('#total').val() + '/' + diterima,
				type : 'GET',
				dataType : 'JSON',
				success : function(data) {
					$('#totalrp').val('Rp. '+ data.totalrp);
					$('#bayarrp').val('Rp. '+ data.bayarrp);
					$('#bayar').val(data.bayar);
					$('#tampil-bayar').text('Bayar: Rp. '+ data.bayarrp);
					$('#tampil-terbilang').text(data.terbilang);

					$('#kembali').val('Rp. '+ data.kembalirp);
					if($('#diterima').val() != 0) {
						$('#tampil-bayar').html('<small>Kembali:</small> Rp. '+ data.kembalirp)
						$('#tampil-terbilang').text(data.kembaliterbilang);
					}
				},
				error : function(data) {
					// alert('Tidak dapat menampilkan data!');
				}
			})
		}
	</script>
@endsection
