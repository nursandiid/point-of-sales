<div class="modal fade" id="modal-member" tabindex="1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; </span></button>
				<h4 class="modal-title">Cari Member</h4>
			</div>

			<div class="modal-body" style="overflow: auto;">
				<table class="table table-striped table-bordered tabel-member">
				    <thead>
				        <th>Kode Member</th>
				        <th>Nama Member</th>
				        <th>Alamat</th>
				        <th>Telpon</th>
				        <th>Aksi</th>
				    </thead>
				
				    <tbody>
				        @foreach($member as $data)
				        	<tr>
				        		<td>{{ $data->kode_member }}</td>
				        		<td>{{ $data->nama }}</td>
				        		<td>{{ $data->alamat }}</td>
				        		<td>{{ $data->telpon }}</td>
				        		<td>
				        			<a onclick="selectMember({{ $data->id_member }}, {{ $data->kode_member }})" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-check-circle"></i> Pilih</a>
				        		</td>
				        	</tr>
				        @endforeach
				    </tbody>
				</table>
			</div>
		</div>
	</div>
</div>