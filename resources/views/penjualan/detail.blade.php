<div class="modal fade" id="modal-detail" tabindex="1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; </span></button>
				<h4 class="modal-title">Detail Penjualan</h4>
			</div>

			<div class="modal-body">
				<table class="table table-striped table-bordered tabel-detail">
				    <thead>
				    	<th width="30">No</th>
				        <th>Kode Produk</th>
				        <th>Nama Produk</th>
				        <th align="right">Harga</th>
				        <th>Jumlah</th>
				        <th align="right">Sub Total</th>
				    </thead>
					
				    <tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>