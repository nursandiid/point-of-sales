<div class="modal fade" id="modal-form" tabindex="1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="fom-horizontal" method="post" data-toggle="validator">
				@csrf {{ method_field('POST') }}
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; </span></button>
					<h4 class="modal-title"></h4>
				</div>

				<div class="modal-body">
					<input type="hidden" id="id" name="id">

					<div class="form-group row">
						<label for="jenis" class="col-md-3 col-md-offset-1 control-label">Jenis Pengeluaran</label>
						<div class="col-md-6">
							<input type="text" name="jenis" id="jenis" class="form-control" required autofocus>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="nominal" class="col-md-3 col-md-offset-1 control-label">Nominal</label>
						<div class="col-md-3">
							<input type="text" name="nominal" id="nominal" class="form-control" required autofocus>
							<span class="help-block with-errors"></span>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-flat btn-primary btn-save"><i class="fa fa-floppy-o"></i>  Simpan</button>
					<button type="button" class="btn btn-sm btn-flat btn-warning" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Batal</button>
				</div>
			</form>
		</div>
	</div>
</div>