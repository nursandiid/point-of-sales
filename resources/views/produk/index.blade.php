@extends('layouts.app')

@section('title')
	Daftar Produk
@endsection

@section('breadcrumb')
	@parent
	<li>Produk</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
			    <div class="box-header with-border">
			        <a onclick="addForm()" class="btn btn-success btn-xs btn-flat"><i class="fa fa-plus-circle"></i> Tambah</a>
			        <a onclick="deleteAll()" class="btn btn-danger btn-xs btn-flat"><i class="fa fa-trash"></i> Hapus</a>
			        <a onclick="printBarcode()" class="btn btn-info btn-xs btn-flat cetak"><i class="fa fa-barcode"></i> Cetak Barcode</a>
			    </div>

			    <form method="post" id="form-produk"> @csrf
		
			    <div class="box-body" style="overflow: auto;">
			        <table class="table table-striped table-bordered">
			            <thead>
			            	<th width="20"><input type="checkbox" value="1" id="select-all" style="cursor: pointer;"></th>
			                <th width="20">No</th>
			                <th>Kode Produk</th>
			                <th>Nama Produk</th>
			                <th>kategori</th>
			                <th>Merk</th>
			                <th>Harga Beli</th>
			                <th>Harga Jual</th>
			                <th>Diskon</th>
			                <th>Stok</th>
			                <th width="100">Aksi</th>
			            </thead>
			            <tbody></tbody>
			        </table>
			    </div>

			    </form>
			</div>
		</div>
	</div>
	@include('produk.form')
@endsection

@section('script')
	<script>
		var table, save_method;
		
		$(function() {
			table = $('.table').DataTable({
				'processing' : true,
				'autoWidth' : false,
				'ajax' : {
					'url' : '{{ route('produk.data') }}',
					'type' : 'GET'
				},
				columnDefs : [{
					'targets' : 0,
					'searchable' : false,
					'orderable' : false
				}],
				'order' : [1, 'asc']
			});
		})

		$('#select-all').click(function() {
			$(':checkbox').prop('checked', this.checked);
		})

		$('.cetak').click(function() {
			$(':checkbox').prop('checked', false);
		})

		$(function() {
			$('#modal-form').validator().on('submit', function(e) {
				if(!e.preventDefault()) {
					var id = $('#id').val();
					
					if(save_method == 'add') url = '{{ route("produk.store") }}';
					else url = 'produk/' + id;

					$.ajax({
						url : url,
						type : 'POST',
						data : $('#modal-form form').serialize(),
						dataType : 'JSON',
						success : function(data) {
							if(data.msg == 'error') {
								alert('Kode produk sudah digunakan');
								$('#kode').focus().select();
							} else {
								$('#modal-form').modal('hide');
								table.ajax.reload();
							}
						}
					})

					return false;
				}
			})
		})

		function passwordRandom() {
			return Math.ceil(Math.random() * 999999)
		}

		function addForm() {
			save_method = 'add';
			$('#modal-form').modal('show');
			$('input[name=_method]').val('POST');
			$('#modal-form form')[0].reset();
			$('.modal-title').text('Tambah Produk');
			$('#modal-form form #nama').focus();
			$('#kode').val({{ date('Ymd') }} +''+ passwordRandom())
		}

		function editForm(id) {
			save_method = 'edit';
			$('input[name=_method]').val('PATCH');
			$('#modal-form form')[0].reset();

			$.ajax({
				url : 'produk/' + id + '/edit',
				type : 'GET',
				dataType : 'JSON',
				success : function(data) {
					$('#modal-form').modal('show');
					$('.modal-title').text('Edit Produk');

					$('#id').val(data.id_produk);
					$('#kode').val(data.kode_produk);
					$('#nama').val(data.nama_produk);
					$('#kategori').val(data.id_kategori);
					$('#merk').val(data.merk);
					$('#harga_beli').val(data.harga_beli);
					$('#diskon').val(data.diskon);
					$('#harga_jual').val(data.harga_jual);
					$('#stok').val(data.stok);
				}, 
				error : function() {
					alert('Tidak dapat menampilkan data');
				}
			})
		}

		function deleteData(id) {
			if(confirm('Apakah yakin data akan dihapus?')) {
				$.ajax({
					url : 'produk/' + id,
					type : 'POST',
					data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
					success : function(data) {
						table.ajax.reload();
					},
					error : function() {
						alert('Tidak dapat menghapus data!');
					}
				})
			}
		}

		function deleteAll() {
			if($('input:checked').length < 1) {
				alert('Pilih data yang akan dihapus');
			} else if (confirm('Apakah yakin akan menghapus semua data yang terpilih ?')) {
				$.ajax({
					url : 'produk/hapus',
					type : 'POST',
					data : $('#form-produk').serialize(),
					success : function(data) {
						table.ajax.reload();
					},
					error : function(data) {
						alert('Tidak dapat menghapus data');
					}
				})
			}
		}

		function printBarcode() {
			if($('input:checked').length < 1) alert('Pilih data yang dicetak');
			else if($('input:checked').length < 3) alert('Minimal 3 data untuk dicetak');
			else $('#form-produk').attr('target', '_blank').attr('action', 'produk/cetak').submit();
		}

	</script>
@endsection
