<div class="modal fade" id="modal-form" tabindex="1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="fom-horizontal" method="post" data-toggle="validator">
				@csrf {{ method_field('POST') }}
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; </span></button>
					<h4 class="modal-title"></h4>
				</div>

				<div class="modal-body">
					<input type="hidden" id="id" name="id">

					<div class="form-group row">
						<label for="kode" class="col-md-2 col-md-offset-1 control-label">Kode Produk</label>
						<div class="col-md-6">
							<input type="number" name="kode" id="kode" class="form-control" required readonly>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="nama" class="col-md-2 col-md-offset-1 control-label">Nama Produk</label>
						<div class="col-md-6">
							<input type="text" name="nama" id="nama" class="form-control" required autofocus>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="kategori" class="col-md-2 col-md-offset-1 control-label">Kategori</label>
						<div class="col-md-6">
							<select name="kategori" id="kategori" class="form-control" required>
								<option value=""> -- Pilih Kategori -- </option>
								@foreach($kategori as $list)
									<option value="{{ $list->id_kategori }}">{{ $list->nama_kategori }}</option>
								@endforeach
							</select>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="merk" class="col-md-2 col-md-offset-1 control-label">Merk</label>
						<div class="col-md-6">
							<input type="text" name="merk" id="merk" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="harga_beli" class="col-md-2 col-md-offset-1 control-label">Harga Beli</label>
						<div class="col-md-6">
							<input type="text" name="harga_beli" id="harga_beli" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="diskon" class="col-md-2 col-md-offset-1 control-label">Diskon</label>
						<div class="col-md-6">
							<input type="text" name="diskon" id="diskon" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="harga_jual" class="col-md-2 col-md-offset-1 control-label">Harga Jual</label>
						<div class="col-md-6">
							<input type="text" name="harga_jual" id="harga_jual" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="stok" class="col-md-2 col-md-offset-1 control-label">Stok</label>
						<div class="col-md-6">
							<input type="text" name="stok" id="stok" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>

				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-flat btn-primary btn-save"><i class="fa fa-floppy-o"></i>  Simpan</button>
					<button type="button" class="btn btn-sm btn-flat btn-warning" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Batal</button>
				</div>
			</form>
		</div>
	</div>
</div>