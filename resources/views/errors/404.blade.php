<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PAGE NOT FOUND !</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .section-body{
            height: 100%;
            position: fixed;
            background: url(https://i.ibb.co/7g9cC8T/error-bg.jpg) center center no-repeat #fff!important;
            width: 100%;
        }
        .section-body h1{
            margin-top: 5%;
            font-size: 210px; 
            font-weight: 900; 
            line-height: 210px;
        }
        .section-body h3,.section-body p,.section-body .btn{margin-top: 1.5%;}
        .section-body .btn{border-radius: 25px;}
        footer.text-center{position: absolute; width: 100%; bottom: 15px;}
    
	@media (max-width: 480px) {
		.section-body h1{ margin-top: 25% }
	}

    </style>

</head>
<body>
    
    <section class="text-center section-body">
        <div>
            <div>
                <h1>404</h1>
                <h3 class="text-uppercase">Page Not Found !</h3>
                <a href="{{ url('/') }}" class="btn btn-info">Back to home</a> 
            </div>
        </div>
    </section>
</body>
</html>
