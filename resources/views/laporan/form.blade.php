<div class="modal fade" id="modal-form" tabindex="1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="fom-horizontal" method="post" data-toggle="validator">
				@csrf
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; </span></button>
					<h4 class="modal-title">Periode Laporan</h4>
				</div>

				<div class="modal-body">
					<div class="form-group row">
						<label for="awal" class="col-md-2 col-md-offset-1 control-label">Tanggal Awal</label>
						<div class="col-md-6">
							<input type="text" name="awal" id="awal" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>
					<div class="form-group row">
						<label for="akhir" class="col-md-2 col-md-offset-1 control-label">Tanggal Akhir</label>
						<div class="col-md-6">
							<input type="text" name="akhir" id="akhir" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-flat btn-primary btn-save"><i class="fa fa-floppy-o"></i>  Simpan</button>
					<button type="button" class="btn btn-sm btn-flat btn-warning" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Batal</button>
				</div>
			</form>
		</div>
	</div>
</div>