@extends('layouts.app')

@section('title')
	Daftar Kategori
@endsection

@section('breadcrumb')
	@parent
	<li>Kategori</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
			    <div class="box-header with-border">
			        <a onclick="addForm()" class="btn btn-success btn-xs btn-flat"><i class="fa fa-plus-circle"></i> Tambah</a>
			    </div>
			
			    <div class="box-body table-responsive">
			        <table class="table table-striped table-bordered">
			            <thead>
			                <th width="30">No</th>
			                <th>Nama Kategori</th>
			                <th width="100">Aksi</th>
			            </thead>
			            <tbody></tbody>
			        </table>
			    </div>
			</div>
		</div>
	</div>
	@include('kategori.form')
@endsection

@section('script')
	<script>
	var table, save_method;
	$(function() {
		table = $('.table').DataTable({
			'processing' : true,
			'autoWidth' : false,
			'ajax' : {
				'url' : '{{ route('kategori.data') }}',
				'type' : 'GET'
			}
		});
	})

		$(function() {
			$('#modal-form').validator().on('submit', function(e) {
				if(!e.preventDefault()) {
					var id = $('#id').val();
					
                    if(save_method == 'add') url = '{{ route("kategori.store") }}';
					else url = 'kategori/' + id;

					$.ajax({
						url : url,
						type : 'POST',
						data : $('#modal-form form').serialize(),
						success : function(data) {
							$('#modal-form').modal('hide');
							table.ajax.reload();
						}
					})

					return false;
				}
			})
		})

		function addForm() {
			save_method = 'add';
			$('#modal-form').modal('show');
			$('input[name=_method]').val('POST');
			$('#modal-form form')[0].reset();
			$('.modal-title').text('Tambah Kategori');
			$('#modal-form form #nama').focus();
		}

		function editForm(id) {
			save_method = 'edit';
			$('input[name=_method]').val('PATCH');
			$('#modal-form form')[0].reset();

			$.ajax({
				url : 'kategori/' + id + '/edit',
				type : 'GET',
				dataType : 'JSON',
				success : function(data) {
					$('#modal-form').modal('show');
					$('.modal-title').text('Edit Kategori');

					$('#id').val(data.id_kategori);
					$('#nama').val(data.nama_kategori);
				}, 
				error : function() {
					alert('Tidak dapat menampilkan data');
				}
			})
		}

		function deleteData(id) {
			if(confirm('Apakah yakin data akan dihapus?')) {
				$.ajax({
					url : 'kategori/' + id,
					type : 'POST',
					data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
					success : function(data) {
						table.ajax.reload();
						if(data.message) alert(data.message);
					},
					error : function(data) {
						alert('Tidak dapat menghapus data!');
					}
				})
			}
		}
	</script>
@endsection
